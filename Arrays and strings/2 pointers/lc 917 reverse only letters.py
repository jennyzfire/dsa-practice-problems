# Given a string s, reverse the string according to the following rules:

# All the characters that are not English letters remain in the same position.
# All the English letters (lowercase or uppercase) should be reversed.
# Return s after reversing it.

# Example 1:
# Input: s = "ab-cd"
# Output: "dc-ba"

# Example 2:
# Input: s = "a-bC-dEf-ghIj"
# Output: "j-Ih-gfE-dCba"

def reverseOnlyLetters(s):
    ans = []
    right = len(ans) - 1
    for i, value in enumerate(s):
        if value.isalpha():
            while not s[right].isalpha():
                right -= 1
            ans.append(s[right])
            right -= 1
        else:
            ans.append(value)

    return "".join(ans)

print(reverseOnlyLetters("a-bC-dEf-ghIj"))
