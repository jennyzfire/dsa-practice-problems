class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        n = len(s)
        ans = 0
        # mp stores the current index of a character
        mp = {}

        i = 0
        # try to extend the range [i, j]
        for j in range(n):
            if s[j] in mp: # if the character is already in the hashmap, it's been seen  before so...
                i = max(mp[s[j]], i) # ...this is incrementing the left window by 1

            ans = max(ans, j - i + 1) # answer will be the largest subwindow length
            mp[s[j]] = j + 1 # this is incrementing the right window by 1
        return ans
