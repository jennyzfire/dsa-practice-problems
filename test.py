def total_song_knowers(num_villagers, attendees_lists):
    villagers = [set() for v in range(num_villagers+1)]
    print("VILLAGERS:", villagers)
    for i in range(len(attendees_lists)):
        curr_night = attendees_lists[i]
        if 0 in attendees_lists[i]:
            for villager in curr_night:
                villagers[villager].add(i)
        else:
            combined = set()
            for villager in curr_night:
                if len(villagers[villager]) > 0:
                    combined.update(villagers[villager])
            for villager in curr_night:
                villagers[villager] = combined.copy()
    minstrel = villagers[0]
    res = 0
    for j in range(1,len(villagers)):
        if len(villagers[j]) == len(minstrel):
            res += 1
    return res

total_song_knowers(5, [[2,4], [0,1,2], [1,3,4], [1,2], [0,5], [3,5]])
