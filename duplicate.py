class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        # [1, 2, 3, 4]
        # return True if any value appears twice in the array
        set_nums = set(nums)
        if len(set_nums) == len(nums):
            return False
        else:
            return True
