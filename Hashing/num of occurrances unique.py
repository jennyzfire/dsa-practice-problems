class Solution:
    def uniqueOccurrences(self, arr: List[int]) -> bool:
        hashmap = {}
        for num in arr:
            if num not in hashmap:
                hashmap[num] = 1
            else:
                hashmap[num] += 1

        if len(set(hashmap.values())) == len(hashmap.values()):
            return True
        else:
            return False
