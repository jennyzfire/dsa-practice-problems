class Solution:
    def largestUniqueNumber(self, nums: List[int]) -> int:
        hashmap = {}
        maxed = -1

        for num in nums:
            if num not in hashmap:
                hashmap[num] = 1
            else:
                hashmap[num] += 1

        for key in hashmap:
            if hashmap[key] == 1:
                if key > maxed:
                    maxed = key
                    print("max num:", maxed)
        print(hashmap)

        return maxed
