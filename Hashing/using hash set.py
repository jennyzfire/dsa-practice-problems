class Solution:
    def numJewelsInStones(self, jewels: str, stones: str) -> int:
        jewels_as_set = set(jewels)
        print(jewels_as_set)
        print(stones)
        return sum(s in jewels_as_set for s in stones)
