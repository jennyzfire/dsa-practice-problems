class Solution:
    def destCity(self, paths: List[List[str]]) -> str:
        # paths = [London, NY], [NY, Lima], [Lima, SP]
        A, B = map(set, zip(*paths))
        print(A)
        print(B)
        print(B - A)
        return (B - A).pop()
