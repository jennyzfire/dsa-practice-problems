import collections

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        if len(ransomNote) > len(magazine):
            return False

        magazine_counts = collections.Counter(magazine)
        print("magazine:", magazine_counts)
        ransom_note_counts = collections.Counter(ransomNote)
        print("ransom note:", ransom_note_counts)

        for char_key, count_value in ransom_note_counts.items():  # .items returns key-value pairs as tuples in a list
            # [(char_key, count_value), (char_key, count_value)]
            magazine_count = magazine_counts[char_key]
            if magazine_count < count_value:
                return False
        return True
