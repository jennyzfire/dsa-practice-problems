class Solution(object):
    def isPathCrossing(self, path):
        """
        :type path: str
        :rtype: bool
        """
        curr = (0, 0)
        visited = set()
        ref = {
            'N': (0,1),
            'S': (0,-1),
            'E': (1,0),
            'W': (-1,0),
        }
        visited.add(curr)

        for char in path:
            dx, dy = ref[char]
            curr_x, curr_y = curr
            new_pos = (curr_x + dx, curr_y + dy)
            # print(new_pos)
            if new_pos in visited:
                return True
            curr = new_pos
            visited.add(curr)

        return False
