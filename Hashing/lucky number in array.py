class Solution:
    def findLucky(self, arr: List[int]) -> int:
        if len(arr) == 0:
            return -1

        lucky = 0

        hashmap = {}
        for num in arr:
            if num not in hashmap:
                hashmap[num] = 1
            else:
                hashmap[num] += 1

        for key in hashmap:
            if key == hashmap[key]:
                if key > lucky:
                    lucky = key

        if lucky == 0:
            return -1

        return lucky
